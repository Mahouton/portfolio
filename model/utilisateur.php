<?php
// Pour la connexion à la base de données
require_once __DIR__ . '/../config/database.php';

//fonction pour connecter l'utilisateur
function se_connecter($mail)
{
    $bdd = connexion();

    //Sélection des données de l'utilisateur
    $sql = $bdd->prepare("SELECT * FROM user WHERE email = '$mail'");
    $sql->execute();

    // récupérer les auteurs dans la variable $data
    $data = $sql->fetch();
    return $data;
}

// Création de compte utilisateur
function createUser($nom, $prenom, $email, $mdp) {
    $bdd = connexion();

    // insertion des données
    $sql = $bdd->prepare("INSERT INTO user(nom, prenom, email, mot_de_passe) VALUES ('".$nom."', '".$prenom."', '".$email."', '".$mdp."')");

    return $sql->execute();
}

function myPortfolio($id)
{
    $bdd = connexion();

    //Sélection des données de l'utilisateur
    $sql = $bdd->prepare("SELECT * FROM user WHERE id = '$id'");
    $sql->execute();

    // récupérer les auteurs dans la variable $data
    $data = $sql->fetch();

    return $data;
}

// fonction pour mettre à jour les données de l'utilisateur connecté
function updateProfil($id, $prenom, $nom, $profil, $description)
{
    $bdd = connexion();

    //Sélection des données de l'utilisateur
    $sql = $bdd->prepare("UPDATE user SET prenom = :prenom, nom = :nom, profil = :profil, description = :description WHERE id = $id");
    $sql->bindParam(':prenom', $prenom);
    $sql->bindParam(':nom', $nom);
    $sql->bindParam(':profil', $profil);
    $sql->bindParam(':description', $description);
    $response = $sql->execute();

    return $response;
}
