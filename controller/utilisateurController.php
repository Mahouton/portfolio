<?php
require_once __DIR__ . '/../model/utilisateur.php';
require_once __DIR__ . '/../config/session.php';

$data = myPortfolio($_GET['id']);

// Vérifier d'abord si y a click sur le bouton update_profil
if (isset($_POST['update_profil'])) {
    $response = updateProfil($_SESSION['id'], $_POST['prenom'], $_POST['nom'], $_POST['profil'], $_POST['description']);

    // vérifier si la requête a fonctionné correctement (avec message de succès ou d'erreur)
    if ($response) {
        header('Location: /view/admin/index.php?portfolio=' .$_SESSION['id'] . '&message=success');
    } else {
        header('Location: index.php?portfolio=' . $_SESSION['id'] . '&message=error');
    }
}


// Appel de la fonction createUser()
if (isset($_POST['creer_compte'])) {

    // Comparer les mots de passes
    if ($_POST["mot_de_passe"] == $_POST["repeter_mdp"]) {
        // Crypter le mot de passe
        $_POST["mot_de_passe"] = password_hash($_POST["repeter_mdp"], PASSWORD_DEFAULT);

        $response = createUser($_POST['nom'], $_POST['prenom'], $_POST['email'], $_POST['mot_de_passe']);

        // vérifier si la requête a fonctionné correctement
        if ($response) {
            //  redirection vers la page de connexion
            header('Location: se_connecter.php?message=create_success');
        } else {
            // redirection vers la page de création de compte avec message d'erreur
            header('Location: index.php?portfolio=' . $_GET['portfolio'] . '&message=create_error');
        }
    } 
    else {
        // redirection vers la page de création de compte avec message d'erreur
        header('Location: creer_compte.php?message=pwd_error');
    }
}
