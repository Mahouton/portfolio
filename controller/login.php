<?php
require_once __DIR__ . '/../model/utilisateur.php';
if (isset($_POST['se_connecter'])) {
    //  Récupérer les données du formulaire
    $mail = $_POST['email'];
    $password = $_POST['mot_de_passe'];
    //  Appel de la fonction se_connecter()
    $data = se_connecter($mail);
    //  Comparer les mots de passe
    if (password_verify($password, $data['mot_de_passe'])) {
        //  Création de la session
        session_start();
        $_SESSION['auth'] = true;
        $_SESSION['id'] = $data['id'];
        $_SESSION['nom'] = $data['nom'];
        $_SESSION['prenom'] = $data['prenom'];
        $_SESSION['profil'] = $data['profil'];
        $_SESSION['email'] = $data['email'];
        $_SESSION['description'] = $data['description'];

        //  Redirection vers la page portfolio
        header('Location: ../portfolio.php?id='.$_SESSION['id']);
        // var_dump($_SESSION['auth']);
    } else {
        //  Redirection vers la page de connexion avec message d'erreur
        header('Location: se_connecter.php?message=error');
    }
}
