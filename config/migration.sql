-- Code pour créer la base de données portfolio et les tables
-- Auteur: Paul KPADONOU
-- Date: 2023-09-04
-- Version: 1.0

-- Création de la base de données
CREATE DATABASE IF NOT EXISTS portfolio;

-- Utilisation de la base de données
USE portfolio;

-- Création de la table users
CREATE TABLE IF NOT EXISTS users (
    id INT(11) NOT NULL AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL,
    nom VARCHAR(255) NOT NULL,
    prenom VARCHAR(255) NOT NULL,
    mot_de_passe VARCHAR(255) NOT NULL,
    reseaux VARCHAR(255),
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Création de la table projets
CREATE TABLE IF NOT EXISTS projets (
    id INT(11) NOT NULL AUTO_INCREMENT,
    titre VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    image VARCHAR(255) NOT NULL,
    id_user INT(11),
    PRIMARY KEY (id),
    FOREIGN KEY (id_user) REFERENCES users(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Création de la table visiteur (ou employeur)
CREATE TABLE IF NOT EXISTS visiteur (
    id INT(11) NOT NULL AUTO_INCREMENT,
    nom_complet VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    message TEXT NOT NULL,
    PRIMARY KEY (id),
    id_user INT(11),
    FOREIGN KEY (id_user) REFERENCES users(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;