<?php
// Connexion à la bd

function connexion()
{

    try {
        $bdd = new  PDO('mysql:host=localhost; dbname=portfolio; charset=utf8', 'admin', 'admin');
    } catch (Exception $e) {
        die('Impossible de se connecter à la BD: ' . $e->getMessage());
    }
    return $bdd;
}
