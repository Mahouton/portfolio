<?php
require_once __DIR__ . '/config/session.php'; 
?> 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="assets/css/styles.css">
</head>
<body  class="body-accueil">
    <div class="block">
    <a class="lien1" href="view/portfolio.php<?= isset($_SESSION['id']) ? '?id='.$_SESSION['id'] : '' ?>">Voir mon portfolio</a>
    <a class="lien2" href="view/admin/index.php">Administrer mon portfolio</a>
    </div>
    
</body>
</html>