<?php 
    require_once __DIR__ . "/../controller/utilisateurController.php";
    require_once __DIR__ . "/../config/security.php";


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>PortFolio</title>
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800&amp;display=swap">
    <link rel="stylesheet" href="../assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/styles.css">
</head>

<body>
    <header>
        <div class="header-background" data-bss-parallax-bg="true" style="height: 600px;background-image: url(&quot;../assets/img/ingenieur.jpg&quot;);background-position: center;background-size: cover;">
            <div class="container d-flex flex-column justify-content-end align-items-start">
                <h1 class="display-3 fw-bold">
                    <!-- Informations à récupérer dynamiquement avec php -->
                    <?= $data['prenom']. " ". $data['nom'] ?>
                    
                </h1>
                <p class="fs-2 text-white text-badge mb-5 bg-secondary fw-bold"><?= $data['profil'] ?></p>
            </div>
        </div>
    </header>
    <main>
        <section class="py-4 py-xl-5">
            <div class="container">
                <p class="fs-3"><?= $data['description'] ?> </p>
                <ul class="list-inline mb-0">
                    <li class="list-inline-item"><i class="fab fa-twitter fs-1 me-2"></i></li>
                    <li class="list-inline-item"><i class="fab fa-facebook fs-1 me-2"></i></li>
                    <li class="list-inline-item"><i class="fab fa-instagram fs-1 me-2"></i></li>
                </ul>
            </div>
        </section>
        <section class="py-4 py-xl-5">
            <div class="container">
                <div class="banner">
                    <p class="fs-1">I am currently <strong>available</strong> for hire.</p><button class="btn btn-dark btn-lg text-uppercase fw-bold" type="button">Hire me&nbsp;<i class="fas fa-chevron-right"></i></button>
                </div>
            </div>
        </section>
        <section class="py-4 py-xl-5">
            <div class="container">
                <h2>Projects</h2>
                <div class="row row-cols-1 row-cols-md-2 row-cols-xl-3 py-5 projects">
                    <div class="col-xl-4">
                        <div class="card"><img class="card-img w-100 d-block" src="assets/img/accueil.png"></div>
                    </div>
                    <div class="col-xl-4">
                        <div class="card"><img class="card-img w-100 d-block" src="assets/img/authentification.png"></div>
                    </div>
                    <div class="col-xl-4">
                        <div class="card"><img class="card-img w-100 d-block" src="assets/img/archiver.png"></div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <footer class="py-3" style="background: var(--bs-gray-900);">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center text-md-start py-2">
                    <div></div>
                    <p class="fs-5 fw-bold mb-0">Crafted by Paul Kpadonou</p>
                    <p class="mb-0">Made with <i class="fas fa-heart"></i>&nbsp;in Benin</p>
                </div>
                <div class="col-md-6 d-flex justify-content-center align-items-center justify-content-md-end">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item"><i class="fab fa-twitter fs-1 me-2"></i></li>
                        <li class="list-inline-item"><i class="fab fa-facebook fs-1 me-2"></i></li>
                        <li class="list-inline-item"><i class="fab fa-instagram fs-1 me-2"></i></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
</body>

</html>
