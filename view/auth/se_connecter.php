<?php
require_once __DIR__ . '/../../controller/login.php';
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Login - Portfolio Admin</title>
    <meta name="description" content="Cette appli web est pour l'administration du portfolio personnel">
    <link rel="icon" type="image/jpeg" sizes="4676x4676" href="../../assets/admin_assets/img/5865.jpg">
    <link rel="stylesheet" href="../../assets/admin_assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;display=swap">
    
    <link rel="stylesheet" href="../../assets/admin_assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="../../assets/admin_assets/css/animate.min.css">
</head>

<body class="bg-gradient-primary">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 col-lg-12 col-xl-10">
                <div class="card shadow-lg o-hidden border-0 my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-flex">
                                <div class="flex-grow-1 bg-login-image" style="background-image: url(&quot;../../assets/admin_assets/img/20944201.jpg&quot;);"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h4 class="text-dark mb-4">Content de te revoir!</h4>
                                        
                                        <?php if (isset($_GET['message']) && $_GET['message'] == 'error') : ?>
                                            <div class="alert alert-danger" role="alert">
                                                L'adresse e-mail ou le mot de passe est incorrect!
                                            </div>
                                        <?php endif; ?>

                                        <!--Message de succès après création de compte-->
                                        <?php if (isset($_GET['message']) && $_GET['message'] == 'create_success') : ?>
                                            <div class="alert alert-success" role="alert">
                                                Votre compte a été créé avec succès! Connectez-vous!
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <form class="user" method="POST">
                                        <div class="mb-3"><input class="form-control form-control-user" type="email" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Entrer l'adresse e-mail..." name="email" required></div>
                                        <div class="mb-3"><input class="form-control form-control-user" type="password" id="exampleInputPassword" placeholder="Mot de passe" name="mot_de_passe" required></div>
                                        <div class="mb-3">
                                            <div class="custom-control custom-checkbox small">
                                                <div class="form-check"><input class="form-check-input custom-control-input" type="checkbox" id="formCheck-1"><label class="form-check-label custom-control-label" for="formCheck-1">Se souvenir de moi</label></div>
                                            </div>
                                        </div><button class="btn btn-primary d-block btn-user w-100" type="submit" name="se_connecter">Se connecter</button>
                                    </form>
                                    <div class="text-center"><a class="small" href="#">Mot de passe oublié?</a></div>
                                    <div class="text-center"><a class="small" href="creer_compte.php">Créer un compte!</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../../assets/admin_assets/js/jquery.min.js"></script>
    <script src="../../assets/admin_assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../assets/admin_assets/js/bs-init.js"></script>
    <script src="../../assets/admin_assets/js/theme.js"></script>
</body>

</html>