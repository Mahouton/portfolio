<?php 
    require_once __DIR__ . "/../../controller/utilisateurController.php";
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Register - Portfolio Admin</title>
    <meta name="description" content="Cette appli web est pour l'administration du portfolio personnel">
    <link rel="icon" type="image/jpeg" sizes="4676x4676" href="../../assets/admin_assets/img/5865.jpg">
    <link rel="stylesheet" href="../../assets/admin_assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;display=swap">
</head>

<body class="bg-gradient-primary">
    <div class="container">
        <div class="card shadow-lg o-hidden border-0 my-5">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-flex">
                        <div class="flex-grow-1 bg-register-image" style="background-image: url(&quot;../../assets/admin_assets/img/register.jpg&quot;);"></div>
                    </div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h4 class="text-dark mb-4">Créer un compte!</h4>
                                
                                <!-- Message d'erreur si les mots de passe ne corrsoondent pas -->
                                <?php if (isset($_GET['message']) && $_GET['message'] == 'pwd_error') : ?>
                                    <div class="alert alert-danger" role="alert">
                                        Les mots de passe ne correspondent pas!
                                    </div>
                                <?php endif; ?>
                            </div>
                            <form class="user" method="post">
                                <div class="row mb-3">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input class="form-control form-control-user" type="text" id="exampleFirstName" placeholder="Prénom(s)" name="prenom" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <input class="form-control form-control-user" type="text" id="exampleLastName" placeholder="Nom" name="nom" required>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <input class="form-control form-control-user" type="email" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Adresse email" name="email" required>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input class="form-control form-control-user" type="password" id="examplePasswordInput" placeholder="Mot de Passe" name="mot_de_passe" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <input class="form-control form-control-user" type="password" id="exampleRepeatPasswordInput" placeholder="Répéter le mot de passe" name="repeter_mdp" required>
                                    </div>
                                </div><button class="btn btn-primary d-block btn-user w-100" type="submit" name="creer_compte">Créer un compte</button>
                                <hr>
                            </form>
                            <div class="text-center"></div>
                            <div class="text-center"><a class="small" href="./se_connecter.php">Vous avez déjà un compte? Se connecter!</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../../assets/admin_assets/js/jquery.min.js"></script>
    <script src="../../assets/admin_assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../assets/admin_assets/js/bs-init.js"></script>
    <script src="../../assets/admin_assets/js/theme.js"></script>
</body>
</html>