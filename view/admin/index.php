<?php
require_once __DIR__ . "/../../controller/utilisateurController.php";
require_once __DIR__ . "/../../config/security.php"; 

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Profile - Portfolio Admin</title>
    <meta name="description" content="Cette appli web est pour l'administration du portfolio personnel">
    <link rel="icon" type="image/jpeg" sizes="4676x4676" href="../../assets/admin_assets/img/5865.jpg">
    <link rel="stylesheet" href="../../assets/admin_assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;display=swap">
    <link rel="stylesheet" href="../../assets/admin_assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="../../assets/admin_assets/css/animate.min.css">
</head>

<body id="page-top">
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0">
            <div class="container-fluid d-flex flex-column p-0"><a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" id="app-name" href="#"><span data-bss-hover-animate="swing"><a href="#"><span style="color: rgb(253, 253, 255);">Portfolio Admin</span></a>&nbsp;</span>
                    <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-briefcase fas fa-briefcase" data-bss-hover-animate="bounce"></i></div>
                    <div class="sidebar-brand-text mx-3"></div>
                </a>
                <hr class="sidebar-divider my-0">
                <ul class="navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item"><a class="nav-link active" href="./?portfolio=1"><i class="fas fa-user"></i><span>Profil</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="./employeur.php"><i class="fas fa-users"></i><span>Employeurs</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="#"><i class="fas fa-project-diagram"></i><span>Projets</span></a></li>
                    <li class="nav-item"><a class="nav-link" target="_blank" href="../../?portfolio=<?= $_GET['portfolio'] ?>"><i class="fas fa-briefcase"></i><span>Portfolio</span></a></li>

                    <li class="nav-item"><a class="nav-link" href="../../controller/logout.php"><i class="fas fa-user-circle"></i><span>Se déconnecter</span></a></li>
                </ul>
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle me-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                        <ul class="navbar-nav flex-nowrap ms-auto">

                            <li class="nav-item dropdown no-arrow mx-1">
                                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><span class="badge bg-danger badge-counter">3+</span><i class="fas fa-bell fa-fw"></i></a>
                                    <div class="dropdown-menu dropdown-menu-end dropdown-list animated--grow-in">
                                        <h6 class="dropdown-header">alerts center</h6>
                                        <a class="dropdown-item d-flex align-items-center" href="#">
                                            <div class="me-3">
                                                <div class="bg-primary icon-circle"><i class="fas fa-file-alt text-white"></i></div>
                                            </div>
                                            <div><span class="small text-gray-500">December 12, 2019</span>
                                                <p>A new monthly report is ready to download!</p>
                                            </div>
                                        </a>

                                        <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item dropdown no-arrow mx-1">
                                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><span class="badge bg-danger badge-counter">7</span><i class="fas fa-envelope fa-fw"></i></a>
                                    <div class="dropdown-menu dropdown-menu-end dropdown-list animated--grow-in">
                                        <h6 class="dropdown-header">alerts center</h6><a class="dropdown-item d-flex align-items-center" href="#">
                                            <div class="dropdown-list-image me-3"><img class="rounded-circle" src="../../assets/admin_assets/img/avatars/avatar4.jpeg">
                                                <div class="bg-success status-indicator"></div>
                                            </div>
                                            <div class="fw-bold">
                                                <div class="text-truncate"><span>Hi there! I am wondering if you can help me with a problem I've been having.</span></div>
                                                <p class="small text-gray-500 mb-0">Emily Fowler - 58m</p>
                                            </div>
                                        </a><a class="dropdown-item d-flex align-items-center" href="#">
                                            <div class="dropdown-list-image me-3"><img class="rounded-circle" src="../../assets/admin_assets/img/avatars/avatar2.jpeg">
                                                <div class="status-indicator"></div>
                                            </div>
                                            <div class="fw-bold">
                                                <div class="text-truncate"><span>I have the photos that you ordered last month!</span></div>
                                                <p class="small text-gray-500 mb-0">Jae Chun - 1d</p>
                                            </div>
                                        </a><a class="dropdown-item d-flex align-items-center" href="#">
                                            <div class="dropdown-list-image me-3"><img class="rounded-circle" src="../../assets/admin_assets/img/avatars/avatar3.jpeg">
                                                <div class="bg-warning status-indicator"></div>
                                            </div>
                                            <div class="fw-bold">
                                                <div class="text-truncate"><span>Last month's report looks great, I am very happy with the progress so far, keep up the good work!</span></div>
                                                <p class="small text-gray-500 mb-0">Morgan Alvarez - 2d</p>
                                            </div>
                                        </a><a class="dropdown-item d-flex align-items-center" href="#">
                                            <div class="dropdown-list-image me-3"><img class="rounded-circle" src="../../assets/admin_assets/img/avatars/avatar5.jpeg">
                                                <div class="bg-success status-indicator"></div>
                                            </div>
                                            <div class="fw-bold">
                                                <div class="text-truncate"><span>Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</span></div>
                                                <p class="small text-gray-500 mb-0">Chicken the Dog · 2w</p>
                                            </div>
                                        </a><a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
                                    </div>
                                </div>
                                <div class="shadow dropdown-list dropdown-menu dropdown-menu-end" aria-labelledby="alertsDropdown"></div>
                            </li>
                            <div class="d-none d-sm-block topbar-divider"></div>
                            <li class="nav-item dropdown no-arrow">
                                <div class="nav-item dropdown no-arrow">
                                    <a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#">
                                        <span class="d-none d-lg-inline me-2 text-gray-600 small">
                                            <?php echo $_SESSION['nom']. " ".$_SESSION['prenom'] ?>
                                        </span>
                                        <img class="border rounded-circle img-profile" src="../../assets/admin_assets/img/avatars/notAvailable.png"></a>
                                    <div class="dropdown-menu shadow dropdown-menu-end animated--grow-in"><a class="dropdown-item" href="#"><i class="fas fa-user fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Profile</a><a class="dropdown-item" href="#"><i class="fas fa-cogs fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Settings</a><a class="dropdown-item" href="#"><i class="fas fa-list fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Activity log</a>
                                        <div class="dropdown-divider"></div><a class="dropdown-item" href="#"><i class="fas fa-sign-out-alt fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Logout</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>

                <div class="container-fluid">
                    <h3 class="text-dark mb-4" data-bss-hover-animate="shake"><strong><span style="color: rgb(16, 16, 17);">Paramètres d'administration</span></strong></h3>
                    <div class="row mb-3">
                        <div class="col-lg-4">
                            <div class="card mb-3">
                                <div class="card-body text-center shadow"><img class="rounded-circle flash animated mb-3 mt-4" src="../../assets/admin_assets/img/avatars/notAvailable.png" width="160" height="160">
                                    <div class="mb-3"><button class="btn btn-primary btn-sm" type="button" name="changer_photo">Changer la photo</button></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">

                            <div class="row">
                                <div class="col">
                                    <div class="card shadow mb-3">
                                        <div class="card-header py-3">
                                            <p class="text-primary m-0 fw-bold">Profil</p>

                                            <!-- Afficher message de succès si la mise à jour a réussi -->
                                            <?php if (isset($_GET['message']) && $_GET['message'] == 'success') : ?>
                                                <div class="alert alert-success" role="alert">
                                                    Vos informations ont été mises à jour avec succès!
                                                </div>
                                            <?php endif; ?>

                                            <!-- Afficher message d'erreur si la mise à jour a échoué -->
                                            <?php if (isset($_GET['message']) && $_GET['message'] == 'error') : ?>
                                                <div class="alert alert-danger" role="alert">
                                                    Une erreur est survenue lors de la mise à jour de vos informations!
                                                </div>
                                            <?php endif; ?>
                                            
                                        </div>
                                        <div class="card-body">
                                            <form method="POST">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="mb-3">
                                                            <label class="form-label" for="prenom"><strong>Prénom</strong></label>
                                                            <input class="form-control" type="text" id="prenom" placeholder="Admin.prénom" name="prenom" value="<?= $_SESSION['prenom'] ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="mb-3">
                                                            <label class="form-label" for="nom"><strong>Nom</strong></label>
                                                            <input class="form-control" type="text" id="nom" placeholder="Admin.nom" name="nom" value="<?= $_SESSION['nom'] ?>" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="mb-3">
                                                            <label class="form-label" for="profil"><strong>Profil</strong></label>
                                                            <input class="form-control" type="text" id="profil" placeholder="Admin.Profil" name="profil" value="<?=$_SESSION['profil']; ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="mb-3">
                                                            <label class="form-label" for="email"><strong>Adresse Email</strong></label>
                                                            <input class="form-control" type="email" id="email" placeholder="admin@example.com" name="email" disabled="" value="<?= $_SESSION['email'] ?>" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="mb-3">
                                                            <label class="form-label" for="description"><strong>Description</strong></label>
                                                            <textarea class="form-control" id="description" name="description" required> <?= $_SESSION['description'] ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mb-3"><button class="btn btn-primary btn-sm" type="submit" name="update_profil">Enregistrer les modifications</button></div>
                                            </form>
                                        </div>
                                    </div>

                                    <!-- Section Réseaux sociaux -->
                                    <div class="card shadow">
                                        <div class="card-header py-3">
                                            <p class="text-primary m-0 fw-bold">Réseaux sociaux</p>
                                        </div>
                                        <div class="card-body">
                                            <form>
                                                <div class="mb-3">
                                                    <label class="form-label" for="linkdin"><strong>LinkdIn&nbsp; &nbsp; &nbsp;</strong></label>
                                                    <input class="form-control" type="text" id="linkdin" placeholder="linkedin.com/user-name" name="reseaux_sociaux">
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label" for="address-3"><strong>Twitter</strong></label>
                                                    <input class="form-control" type="text" id="address-3" placeholder="twitter.com/user-name" name="reseaux_sociaux">
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label" for="address-2"><strong>Facebook</strong></label>
                                                    <input class="form-control" type="url" id="address-2" placeholder="facebook.com/user-name" name="reseaux_sociaux">
                                                </div>
                                                <!-- <div class="mb-3"></div> -->
                                                <div class="mb-3"><button class="btn btn-primary btn-sm" type="submit" name="enregistrer_reseaux">Enregistrer</button></div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card shadow mb-5"></div>
                </div>
            </div>
            <footer class="bg-white sticky-footer">
                <div class="container my-auto">
                    <div class="text-center my-auto copyright"><span>Copyright © Portfolio Admin 2023</span></div>
                </div>
            </footer>
        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>
    <script src="../../assets/admin_assets/js/jquery.min.js"></script>
    <script src="../../assets/admin_assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../assets/admin_assets/js/bs-init.js"></script>
    <script src="../../assets/admin_assets/js/theme.js"></script>
</body>

</html>