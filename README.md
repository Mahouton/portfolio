## Description du projet
Ce projet est un projet de fin de formation, il a pour but de mettre en pratique les connaissances acquises durant la formation.

Il s'agit d'un site web  portfolio qui présente les projets réalisés durant la formation ainsi que les compétences acquises. Ce site dispose d'un espace d'administration qui permet de gérer les projets et les informations personnelles.

## Comment créer  ou exporter la base de données depuis le fichier migration.sql  dans phpmyadmin
1. Créer une base de données nommée portfolio
2. Importer le fichier migration.sql dans la base de données portfolio

## Comment configurer le fichier config.php
1. Ouvrir le fichier config.php
2. Modifier les constantes DB_HOST, DB_NAME, DB_USER, DB_PASSWORD en fonction de votre configuration

## Comment configurer le fichier .htaccess
1. Ouvrir le fichier .htaccess


